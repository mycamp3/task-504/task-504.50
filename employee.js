class Employee {
    constructor(firstname, lastname, socialSecurityNumber) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    getFullName() {
        return `${this.firstname} ${this.lastname}`;
    }

    getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }

    setSocialSecurityNumber(socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
}

export { Employee}