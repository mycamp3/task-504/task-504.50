import { CommissionEmployee } from "./commissionEmployee.js";
class BasePlusCommissionEmployee extends CommissionEmployee {
    constructor(firstname, lastname, socialSecurityNumber, grossSale, commissionRate, baseSalary) {
        super(firstname, lastname, socialSecurityNumber, grossSale, commissionRate);
        this.baseSalary = baseSalary;
    }

    getBaseSalary() {
        return this.baseSalary;
    }

    setBaseSalary(baseSalary) {
        this.baseSalary = baseSalary;
    }

    calculateEarnings() {
        return super.calculateEarnings() + this.baseSalary;
    }

    getFullInfo() {
        return `${this.getFullName()}, SSN: ${this.getSocialSecurityNumber()}, Gross Sale: ${this.grossSale}, Commission Rate: ${this.commissionRate}, Base Salary: ${this.baseSalary}, Earnings: ${this.calculateEarnings()}`;
    }
}

export {BasePlusCommissionEmployee}
