import { Employee } from "./employee.js";
class SalariedEmployee extends Employee {
    constructor(firstname, lastname, socialSecurityNumber, weeklySalary) {
        super(firstname, lastname, socialSecurityNumber);
        this.weeklySalary = weeklySalary;
    }

    getWeeklySalary() {
        return this.weeklySalary;
    }

    setWeeklySalary(weeklySalary) {
        this.weeklySalary = weeklySalary;
    }

    calculateEarnings() {
        return this.weeklySalary;
    }

    getFullInfo() {
        return `${this.getFullName()}, SSN: ${this.getSocialSecurityNumber()}, Weekly Salary: ${this.weeklySalary}, Earnings: ${this.calculateEarnings()}`;
    }
}
export { SalariedEmployee}
