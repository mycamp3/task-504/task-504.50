import { Employee } from "./employee.js";
import { CommissionEmployee } from "./commissionEmployee.js";
import { HourlyEmployee } from "./hourlyEmployee.js";
import { SalariedEmployee } from "./salariedEmployee.js";
import { BasePlusCommissionEmployee } from "./basePlusCommissionEmployee.js";

const employee1 = new Employee('John', 'Doe', '123-45-6789');

// Gọi các phương thức để kiểm tra
console.log(employee1.getFullName()); // John Doe
console.log(employee1.getSocialSecurityNumber()); // 123-45-6789
employee1.setSocialSecurityNumber('987-65-4321');
console.log(employee1.getSocialSecurityNumber()); // 987-65-4321

// Kiểm tra đối tượng bằng instanceof
console.log(employee1 instanceof Employee); // true



const commissionEmployee1 = new CommissionEmployee('Alice', 'Johnson', '555-55-5555', 10000, 0.1);

// Gọi các phương thức để kiểm tra
console.log(commissionEmployee1.getFullName());
console.log(commissionEmployee1.getSocialSecurityNumber()); // 555-55-5555
console.log(commissionEmployee1.getGrossSale()); // 10000
console.log(commissionEmployee1.getCommissionRate()); // 0.1
console.log(commissionEmployee1.calculateEarnings()); // 1000
console.log(commissionEmployee1.getFullInfo()); // Alice Johnson, SSN: 555-55-5555, Gross Sale: 10000, Commission Rate: 0.1, Earnings: 1000

// Sử dụng các phương thức set để thay đổi thông tin
commissionEmployee1.setGrossSale(20000);
commissionEmployee1.setCommissionRate(0.15);

console.log(commissionEmployee1.getGrossSale()); // 20000
console.log(commissionEmployee1.getCommissionRate()); // 0.15
console.log(commissionEmployee1.calculateEarnings()); // 3000
console.log(commissionEmployee1.getFullInfo()); // Alice Johnson, SSN: 555-55-5555, Gross Sale: 20000, Commission Rate: 0.15, Earnings: 3000

// Kiểm tra đối tượng bằng instanceof
console.log(commissionEmployee1 instanceof CommissionEmployee); // true
console.log(commissionEmployee1 instanceof Employee); // true


const hourlyEmployee1 = new HourlyEmployee('Bob', 'Smith', '111-22-3333', 20, 40);


console.log(hourlyEmployee1.getFullName());
console.log(hourlyEmployee1.getSocialSecurityNumber());
console.log(hourlyEmployee1.getWage());
console.log(hourlyEmployee1.getHours());
console.log(hourlyEmployee1.calculateEarnings());
console.log(hourlyEmployee1.getFullInfo());
hourlyEmployee1.setWage(25);
hourlyEmployee1.setHours(35);

console.log(hourlyEmployee1.getWage());
console.log(hourlyEmployee1.getHours());
console.log(hourlyEmployee1.calculateEarnings());
console.log(hourlyEmployee1.getFullInfo());


const salariedEmployee1 = new SalariedEmployee('Alice', 'Johnson', '444-55-6666', 1500);

console.log(salariedEmployee1.getFullName());
console.log(salariedEmployee1.getSocialSecurityNumber());
console.log(salariedEmployee1.getWeeklySalary());
console.log(salariedEmployee1.calculateEarnings());
console.log(salariedEmployee1.getFullInfo());

salariedEmployee1.setWeeklySalary(2000);

console.log(salariedEmployee1.getWeeklySalary());
console.log(salariedEmployee1.calculateEarnings());
console.log(salariedEmployee1.getFullInfo());

console.log(hourlyEmployee1 instanceof HourlyEmployee); // true
console.log(hourlyEmployee1 instanceof Employee); // true
console.log(salariedEmployee1 instanceof SalariedEmployee); // true
console.log(salariedEmployee1 instanceof Employee); // true


const basePlusCommissionEmployee1 = new BasePlusCommissionEmployee('John', 'Doe', '777-88-9999', 15000, 0.2, 3000);

console.log(basePlusCommissionEmployee1.getFullName());
console.log(basePlusCommissionEmployee1.getSocialSecurityNumber());
console.log(basePlusCommissionEmployee1.getGrossSale());
console.log(basePlusCommissionEmployee1.getCommissionRate());
console.log(basePlusCommissionEmployee1.getBaseSalary());
console.log(basePlusCommissionEmployee1.calculateEarnings());
console.log(basePlusCommissionEmployee1.getFullInfo());

basePlusCommissionEmployee1.setGrossSale(20000);
basePlusCommissionEmployee1.setCommissionRate(0.25);
basePlusCommissionEmployee1.setBaseSalary(3500);

console.log(basePlusCommissionEmployee1.getGrossSale());
console.log(basePlusCommissionEmployee1.getCommissionRate());
console.log(basePlusCommissionEmployee1.getBaseSalary());
console.log(basePlusCommissionEmployee1.calculateEarnings());
console.log(basePlusCommissionEmployee1.getFullInfo());

console.log(basePlusCommissionEmployee1 instanceof BasePlusCommissionEmployee); // true
console.log(basePlusCommissionEmployee1 instanceof CommissionEmployee); // true
console.log(basePlusCommissionEmployee1 instanceof Employee); // true
