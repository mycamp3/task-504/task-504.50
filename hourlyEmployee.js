import { Employee } from "./employee.js";
class HourlyEmployee extends Employee {
    constructor(firstname, lastname, socialSecurityNumber, wage, hours) {
        super(firstname, lastname, socialSecurityNumber);
        this.wage = wage;
        this.hours = hours;
    }

    getWage() {
        return this.wage;
    }

    getHours() {
        return this.hours;
    }

    setWage(wage) {
        this.wage = wage;
    }

    setHours(hours) {
        this.hours = hours;
    }

    calculateEarnings() {
        return this.wage * this.hours;
    }

    getFullInfo() {
        return `${this.getFullName()}, SSN: ${this.getSocialSecurityNumber()}, Wage: ${this.wage}, Hours: ${this.hours}, Earnings: ${this.calculateEarnings()}`;
    }
}

export { HourlyEmployee}