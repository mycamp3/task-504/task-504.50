import { Employee } from "./employee.js";
class CommissionEmployee extends Employee {
    constructor(firstname, lastname, socialSecurityNumber, grossSale, commissionRate) {
        super(firstname, lastname, socialSecurityNumber);
        this.grossSale = grossSale;
        this.commissionRate = commissionRate;
    }

    getGrossSale() {
        return this.grossSale;
    }

    getCommissionRate() {
        return this.commissionRate;
    }

    setGrossSale(grossSale) {
        this.grossSale = grossSale;
    }

    setCommissionRate(commissionRate) {
        this.commissionRate = commissionRate;
    }

    calculateEarnings() {
        return this.grossSale * this.commissionRate;
    }

    getFullInfo() {
        return `${this.getFullName()}, SSN: ${this.getSocialSecurityNumber()}, Gross Sale: ${this.grossSale}, Commission Rate: ${this.commissionRate}, Earnings: ${this.calculateEarnings()}`;
    }
}

export { CommissionEmployee}